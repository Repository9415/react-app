## Form Authorization

#### software requirements
```
IDE WebStorm
NodeJS
```

#### technology stack
```
JavaScript
React
HTML
CSS
```

#### developer
```
name: Ivan
email: ivan@gmail.com
```

#### build application
```
npm install
npm run build
```

#### run application
```
npm start
```
