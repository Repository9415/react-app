import React from 'react'
import {BrowserRouter, Route, Redirect} from 'react-router-dom'
import {MainPage} from './components/MainPage'
import {LoginForm} from './components/LoginForm'
import './App.css'

function App() {
    return (
        <BrowserRouter>
            <Route path="/" exact>
                <MainPage/>
            </Route>
            <Route path="/logined" exact>
                <LoginForm/>
            </Route>
            <Redirect to="/"/>
        </BrowserRouter>
    )
}

export default App