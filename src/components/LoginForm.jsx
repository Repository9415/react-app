import React, {useState} from 'react'
import {Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

export const LoginForm = (props) => {

    const [form, setForm] = useState({
        username: '',
        password: '',
    })

    const [error, setError] = useState({
        message: ''
    })

    const changeHandler = event => {
        setForm({...form, [event.target.name]: event.target.value});
    }

    const loginHandler = async (event) => {
        event.preventDefault()
        try {
            if (form.username && form.password) {
                const success = form.username === 'admin' && form.password === 'password' ? true : false
                if (success) {
                    return props.onLogin(true)
                }
                setError({message: 'Wrong login or password!'})
            } else {
                setError({message: 'Please fill in the required fields!'})
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <div className="authpage-container">
            <div className="login-page">
                <div className="form">
                    <form className="login-form">
                        <TextField
                            type="text"
                            name="username"
                            placeholder="name"
                            onChange={changeHandler}
                            value={form.username}
                            fullWidth
                            required
                        />
                        <TextField
                            type="password"
                            name="password"
                            placeholder="password"
                            onChange={changeHandler}
                            value={form.password}
                            fullWidth
                            required
                        />
                        {error.message ? <p className="message">{error.message}</p> : error.message}
                        <Button
                            type="button"
                            onClick={loginHandler}>login</Button>
                    </form>
                </div>
            </div>
        </div>
    )
}