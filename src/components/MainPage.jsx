import React, {useState, useCallback} from 'react'
import {LoginForm} from './LoginForm'
import {Button} from "@material-ui/core";

export const MainPage = () => {
    const [logined, setLogined] = useState(false)

    const logoutHandler = () => {
        setLogined(false)
    }

    const logingHandler = useCallback(() => {
        setLogined(true)
    }, [setLogined])

    const content = (
        <div className="login-page">
            <div className="form">
                <Button variant="text" onClick={logoutHandler}>Exit</Button>
            </div>
        </div>
    )

    return (
        logined ? content : <LoginForm onLogin={logingHandler}/>
    )
}